package me.pixka.hrcheckservice.c

import me.pixka.hrcheckservice.o.CheckinObject
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController
import java.util.*


@RestController
class GetCheckinControl
{

    @Autowired
    lateinit var kafkaTemplate: KafkaTemplate<String, CheckinObject>
    @GetMapping("/rest/checkin/{tag}")
    fun getCheckin(@PathVariable tag:String)
    {

        println(tag)
        kafkaTemplate.send("checkin", CheckinObject(tag, Date()))

    }
}