package me.pixka.hrcheckservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class HrcheckserviceApplication

fun main(args: Array<String>) {
	runApplication<HrcheckserviceApplication>(*args)
}
